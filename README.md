# Table des matières

1. [Projet Rest](#projet-rest)
2. [Contributeurs](#contributeurs)
3. [Installation](#installation)
    - [Pré-requis](#pre-requis)
    - [Installation Manuelle](#installation-manuelle)
    - [Installation via Docker](#installation-via-docker)
4. [Fonctionnalités](#fonctionnalités)
    - [Globales](#globales)

# Projet Rest

<!-- Le projet ```Rest``` a pour but de mettre à disposition une liste de musiques regroupées ou non dans des playlists. Chaque musique possède une page de détails, peut être jouée et entièrement customisable à même titre que les playlists. Egalement, trois status d'utilisateurs sont distinguables :
- les visiteurs, qui peuvent simplement écouter les playlists
- les administrateurs, qui peuvent ajouter/modifier/supprimer les playlists et musiques et gérer les visiteurs
- les super-administrateurs, qui peuvent tout gérer et nommer des administrateurs
```Swing``` est réalisé dans le cadre d'un projet informatique à l' [IUT Informatique d'Orléans](https://www.univ-orleans.fr/fr/iut-orleans) et met en application une API REST basée sur Symfony 5, SvelteJS et SQLite et entièrement déployable via Docker. -->

## Contributeurs

- [DURAND Romain](https://gitlab.com/526f6d6172696e)

## Installation

> ### Pre-requis

[Composer](https://getcomposer.org/) [Symfony](https://symfony.com/) [NodeJS](https://nodejs.org/en/) et/ou [Docker](https://www.docker.com/) sont requis pour lancer ce projet. 

### Installation manuelle

1. Dans un premier temps, placez-vous dans votre dossier favori prêt à accueillir l'application et récupérez le projet [Rest](https://gitlab.com/526f6d6172696e/projet-forum) via la commande :
    ```bash
    git clone https://gitlab.com/526f6d6172696e/projet-forum
    ```

2. Ensuite, installez les dépendances du projet :
    ```bash
    cd projet-forum
    composer install
    npm i
    npm run dev
    ```

3. Maintenant, il faut gérer la base de données. Nous allons donc la créer puis effectuer les modifications nécessaires sur celle-ci :
    ```bash
    symfony console doctrine:database:create
    symfony console doctrine:migrations:migrate --no-interaction
    ```

4. Lançons maintenant le serveur symfony :
    ```bash
    symfony serve -d
    ```

Le site [Rest](http://localhost:8000) est désormais accessible !

### Installation via docker

1. Identique à l'installation manuelle
2. Décommentez la ligne suivante dans le fichier .env
    ```bash
    DATABASE_URL="mysql://db_user:loulou41@rest_db/rest?serverVersion=5.7"
    ```
3. Commentez la ligne suivante dans le fichier .env
    ```bash
    # DATABASE_URL="sqlite:///%kernel.project_dir%/var/data.db"
    ```
4. Lançons nos containers. Cela peut durer 5 minutes.
    ```bash
    cd projet-forum
    docker-compose up -d
    ```
5. Attendez 1 minute supplémentaire.

Le site [Rest](http://localhost:8000) est désormais accessible !

## Fonctionnalités

> ### Globales

Depuis notre site il est possible :
- de visionner des sujets et leur réponse (```visiteur```)
- d'accéder à toutes les catégories (```visiteur```)
- d'ajouter des catégories / sujets et de répondre au sujet (```Utilisateur```)
- ajouter / suppimer des utilisateurs (```administrateur```)