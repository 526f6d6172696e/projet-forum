<?php

namespace App\Controller;

use App\Entity\User;
use Symfony\Component\Security\Http\Attribute\CurrentUser;
use App\Repository\UserRepository;
use App\Security\AppAuthenticator;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\AuthenticationManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Security\Http\Authentication\UserAuthenticatorInterface;

class SecurityController extends AbstractController
{
    private AuthenticationManagerInterface $authenticationManager;
    private TokenStorageInterface $tokenStorage;

    function __construct(
        AuthenticationManagerInterface $authenticationManager,
        TokenStorageInterface $tokenStorage
    ) {
        $this->authenticationManager = $authenticationManager;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @Route("/login", name="app_auth_login")
     */
    public function login(Request $request, UserRepository $userRepository, UserPasswordEncoderInterface $encoder)
    {
        $body = json_decode($request->getContent(), true);
        $nom = $body["username"];
        $password = $body["password"];

        $user = $userRepository->findOneBy(array("username" => $nom));
        $response = array("success" => 0);

        if ($user)
            if ($encoder->isPasswordValid($user, $password)) {
                $token = new UsernamePasswordToken($user, null, 'main', $user->getRoles());
                $authenticatedToken = $this->authenticationManager->authenticate($token);
                $this->tokenStorage->setToken($authenticatedToken);

                $response = array(
                    "success" => 1,
                    "id" => $user->getId(),
                    "nom" => $user->getUserIdentifier(),
                    "roles" => $user->getRoles(),
                );
            }

        // On renvoie l'utilisateur demandé
        $reponse = new Response(json_encode($response));

        $reponse->headers->set("Content-Type", "application/json");
        $reponse->headers->set("Access-Control-Allow-Origin", "*");
        return $reponse;
    }

    /**
     * @Route("/register", name="app_register")
     */
    public function register(
        Request $request,
        EntityManagerInterface $entityManager,
        UserPasswordEncoderInterface $encoder,
        UserRepository $userRepository
    ): Response {
        $body = json_decode($request->getContent(), true);
        $nom = $body["username"];
        $password = $body["password"];
        $response = array("success" => 0);

        if (!$userRepository->findOneBy(array("username" => $nom))) {
            $user = new User();

            $user->setUsername($nom);
            $user->setPassword($encoder->encodePassword($user, $password));
            $user->setDateInscription(new DateTime());

            if ($nom != "Romain") {
                $user->setRoles(array(
                    "ROLE_USER"
                ));
            } else {
                $user->setRoles(array(
                    "ROLE_ADMIN"
                ));
            }



            $entityManager->persist($user);
            $entityManager->flush();

            $token = new UsernamePasswordToken($user, null, 'main', $user->getRoles());
            $authenticatedToken = $this->authenticationManager->authenticate($token);
            $this->tokenStorage->setToken($authenticatedToken);

            $response = array(
                "success" => 1,
                "id" => $user->getId(),
                "nom" => $user->getUserIdentifier(),
                "roles" => $user->getRoles(),
            );
        }

        // On renvoie l'utilisateur demandé
        $reponse = new Response(json_encode($response));

        $reponse->headers->set("Content-Type", "application/json");
        $reponse->headers->set("Access-Control-Allow-Origin", "*");
        return $reponse;
    }

    /**
     * @Route("/userCreate")
     */
    public function createUser(
        Request $request,
        EntityManagerInterface $entityManager,
        UserPasswordEncoderInterface $encoder,
        UserRepository $userRepository
    ): Response {
        $body = json_decode($request->getContent(), true);
        $nom = $body["username"];
        $password = $body["password"];
        $response = array("success" => 0);

        if (!$userRepository->findOneBy(array("username" => $nom))) {
            $user = new User();

            $user->setUsername($nom);
            $user->setPassword($encoder->encodePassword($user, $password));
            $user->setDateInscription(new DateTime());

            $entityManager->persist($user);
            $entityManager->flush();

            $response = array(
                "success" => 1,
                "id" => $user->getId(),
                "nom" => $user->getUserIdentifier(),
                "roles" => $user->getRoles(),
            );
        }

        // On renvoie l'utilisateur demandé
        $reponse = new Response(json_encode($response));

        $reponse->headers->set("Content-Type", "application/json");
        $reponse->headers->set("Access-Control-Allow-Origin", "*");

        return $reponse;
    }

    /**
     * @Route("/allUsers")
     */
    public function getUsers(UserRepository $userRepository): Response
    {
        $users = $userRepository->findAll();
        $data = array();

        foreach ($users as $user) {
            array_push($data, array(
                "id" => $user->getId(),
                "username" => $user->getUserIdentifier(),
                "roles" => $user->getRoles(),
                "date" => $user->getDateInscription(),
            ));
        }
        $reponse = new Response();
        $reponse->setContent(json_encode($data));
        $reponse->headers->set("Content-Type", "application/json");
        $reponse->headers->set("Access-Control-Allow-Origin", "*");

        return $reponse;
    }

    /**
     * @Route("/logout", name="app_logout")
     */
    public function logout(): Response
    {
        $this->tokenStorage->setToken(null);

        if ($this->tokenStorage->getToken()) $success = 0;
        else $success = 1;

        // On renvoie l'utilisateur demandé
        $reponse = new Response(json_encode(array(
            "success" => $success
        )));

        $reponse->headers->set("Content-Type", "application/json");
        $reponse->headers->set("Access-Control-Allow-Origin", "*");
        return $reponse;
    }

    /**
     * @Route("/user/delete")
     */
    public function deleteCurrentUser(EntityManagerInterface $entityManager)
    {
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $this->tokenStorage->setToken(null);

        $entityManager->remove($user);
        $entityManager->flush();

        return $this->json([
            'success' => 1
        ]);
    }

    /**
     * @Route("/user/delete/{id}")
     */
    public function deleteUser(int $id, UserRepository $userRepository, EntityManagerInterface $entityManager)
    {
        $user = $userRepository->findOneBy(array('id' => $id));
        $entityManager->remove($user);
        $entityManager->flush();

        return $this->json([
            'success' => 1
        ]);
    }
}
