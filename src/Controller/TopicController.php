<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\Topic;
use App\Form\Topic1Type;
use App\Repository\TopicRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use DateTime;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * @Route("/topic")
 */
class TopicController extends AbstractController
{
    private TokenStorageInterface $tokenStorage;

    function __construct(
        TokenStorageInterface $tokenStorage
    ) {
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @Route("/new/{id}", name="topic_new", methods={"GET", "POST"})
     */
    public function new(int $id, Request $request, EntityManagerInterface $entityManager): Response
    {

        $user = $this->tokenStorage->getToken()->getUser();
        $body = json_decode($request->getContent(), true);

        $topic = new Topic();
        $categ = $entityManager->getRepository(Category::class)->findOneBy(array('id' => $id));
        $topic->setCategory($categ);
        $topic->setCreator($user);
        $date = new DateTime();
        $topic->setTimestamp($date->format('c'));

        $topic->setTitle($body['title']);
        $topic->setMessage($body["message"]);
        $topic->setLikes(0);
        $topic->setViews(0);

        $entityManager->persist($topic);
        $entityManager->flush();

        $reponses = array(
            "id" =>         $topic->getId(),
            "author" =>     $topic->getCreator()->getUserIdentifier(),
            "timestamp" =>  $topic->getTimestamp(),
            "likes" =>      $topic->getLikes(),
            "message" =>    $topic->getMessage()

        );

        $reponse = new Response();
        $reponse->setContent(json_encode($reponses));
        $reponse->headers->set("Content-Type", "application/json");
        $reponse->headers->set("Access-Control-Allow-Origin", "*");

        return $reponse;
    }

    /**
     * @Route("/responses/{id}", name="topic_responses", methods={"GET"})
     */
    public function getResponse(int $id, TopicRepository $topicRepository): Response
    {
        $topics = $topicRepository->findById($id);

        $data = array();
        $reponses = array();

        foreach ($topics as $topic) {
            $this->getDoctrine()->getManager()->persist($topic);
            $topic->setViews($topic->getViews() + 1);

            foreach ($topic->getReponses() as $response) {
                array_push($reponses, array(
                    "id" =>         $response->getId(),
                    "author" =>     $response->getAuthor()->getUserIdentifier(),
                    "timestamp" =>  $response->getTimestamp(),
                    "likes" =>      $response->getLikes(),
                    "message" =>    $response->getMessage()

                ));
            }
            array_push($data, array(
                "id" =>         $topic->getId(),
                "author" =>     $topic->getCreator()->getUserIdentifier(),
                "timestamp" =>  $topic->getTimestamp(),
                "likes" =>      $topic->getLikes(),
                "views" =>      $topic->getViews(),
                "title" =>      $topic->getTitle(),
                "message" =>    $topic->getMessage(),
                'category' =>   $topic->getCategory()->getId(),
                "reponses" =>   $reponses

            ));
        }
        $this->getDoctrine()->getManager()->flush();

        $reponse = new Response();
        $reponse->setContent(json_encode($data));
        $reponse->headers->set("Content-Type", "application/json");
        $reponse->headers->set("Access-Control-Allow-Origin", "*");

        return $reponse;
    }
}
