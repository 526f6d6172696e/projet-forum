<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;

class DefaultController extends AbstractController
{
    /**
     * @Route("/", name="accueil")
     */
    public function index(): Response
    {
        return $this->render('index.html.twig');
    }

    /**
     * @Route("/user")
     */
    public function userInterraction()
    {
        $user = $this->get('security.token_storage')->getToken()->getUser();

        return $this->json([
            'username' => $user->getUserIdentifier(),
            'icon' => $user->getIcone(),
            'dateInsc' => $user->getDateInscription(),
            'roles' => $user->getRoles(),
        ]);
    }

    /**
     * @Route("/user/modifIcon")
     */
    public function userIcon(Request $request, EntityManagerInterface $entityManager)
    {
        $body = json_decode($request->getContent(), true);
        $icon = $body["icon"];

        $user = $this->get('security.token_storage')->getToken()->getUser();

        $entityManager->persist($user);
        $user->setIcone($icon);
        $entityManager->flush();

        return $this->json([
            'success' => 1
        ]);
    }
}
