<?php

namespace App\Controller;

use App\Entity\Reponse;
use App\Entity\Topic;
use App\Form\ReponseType;
use App\Repository\ReponseRepository;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * @Route("/reponse")
 */
class ReponseController extends AbstractController
{
    private TokenStorageInterface $tokenStorage;

    function __construct(
        TokenStorageInterface $tokenStorage
    ) {
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @Route("/new/{id}", name="reponse_new", methods={"GET", "POST"})
     */
    public function new(int $id, Request $request, EntityManagerInterface $entityManager): Response
    {
        $user = $this->tokenStorage->getToken()->getUser();
        $body = json_decode($request->getContent(), true);

        if ($user) {
            $reponse = new Reponse();

            $topic = $entityManager->getRepository(Topic::class)->findOneBy(array('id' => $id));
            $reponse->setTopic($topic);
            $reponse->setAuthor($user);
            $reponse->setMessage($body["message"]);
            $date = new DateTime();
            $reponse->setTimestamp($date->format('c'));
            $reponse->setLikes(0);

            $entityManager->persist($reponse);
            $entityManager->flush();
        }

        $reponses = array(
            "id" =>         $reponse->getId(),
            "author" =>     $reponse->getAuthor()->getUserIdentifier(),
            "timestamp" =>  $reponse->getTimestamp(),
            "likes" =>      $reponse->getLikes(),
            "message" =>    $reponse->getMessage()

        );

        $reponse = new Response();
        $reponse->setContent(json_encode($reponses));
        $reponse->headers->set("Content-Type", "application/json");
        $reponse->headers->set("Access-Control-Allow-Origin", "*");

        return $reponse;
    }
}
