<?php

namespace App\Controller;

use App\Entity\Category;
use App\Form\CategoryType;
use App\Repository\CategoryRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Entity\Topic;
use App\Entity\User;

/**
 * @Route("/category")
 */
class CategoryController extends AbstractController
{
    /**
     * @Route("/", name="category_index", methods={"GET"})
     */
    public function index(CategoryRepository $categoryRepository): Response
    {
        $category = $categoryRepository->findAll();
        $data = array();

        foreach ($category as $categorie) {
            array_push($data, array(
                "id" =>          $categorie->getId(),
                "title" =>       $categorie->getTitle(),
                "image" =>       $categorie->getImage(),
                "description" => $categorie->getDescription()
            ));
        }
        $reponse = new Response();
        $reponse->setContent(json_encode($data));
        $reponse->headers->set("Content-Type", "application/json");
        $reponse->headers->set("Access-Control-Allow-Origin", "*");

        return $reponse;
    }

    /**
     * @Route("/new", name="category_new", methods={"GET", "POST"})
     */
    public function new(Request $request, EntityManagerInterface $entityManager)
    {
        $body = json_decode($request->getContent(), true);
        $title = $body["title"];
        $img = $body["img"];
        $desc = $body["desc"];

        $category = new Category();
        $category->setTitle($title);
        $category->setImage($img);
        $category->setDescription($desc);

        $entityManager->persist($category);
        $entityManager->flush();

        return $this->json(
            [
                "success" => 1
            ]
        );
    }

    /**
     * @Route("/detail/{id}", name="category_detail", methods={"GET"})
     */
    public function detail(int $id, CategoryRepository $categoryRepository): Response
    {
        $category = $categoryRepository->findById($id);

        $data = array();
        $topics = array();
        foreach ($category as $categorie) {
            foreach ($categorie->getTopics() as $topic) {
                array_push($topics, array(
                    "id" =>         $topic->getId(),
                    "author" =>     $topic->getCreator()->getUserIdentifier(),
                    "timestamp" =>  $topic->getTimestamp(),
                    "likes" =>      $topic->getLikes(),
                    "views" =>      $topic->getViews(),
                    "title" =>      $topic->getTitle(),
                    "message" =>    $topic->getMessage()

                ));
            }
            array_push($data, array(
                "id" =>          $categorie->getId(),
                "title" =>       $categorie->getTitle(),
                "image" =>       $categorie->getImage(),
                "description" => $categorie->getDescription(),
                "topics" =>      $topics
            ));
        }
        $reponse = new Response();
        $reponse->setContent(json_encode($data));
        $reponse->headers->set("Content-Type", "application/json");
        $reponse->headers->set("Access-Control-Allow-Origin", "*");

        return $reponse;
    }
}
