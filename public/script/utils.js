/**
 * check if it's a valid URL
 * @param {string} str 
 * @returns 
 */
function validURL(str) {
    var pattern = new RegExp('^(https?:\\/\\/)?' + // protocol
        '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
        '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
        '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
        '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
        '(\\#[-a-z\\d_]*)?$', 'i'); // fragment locator
    return !!pattern.test(str);
}

/**
 * Allow the user to modify his icon profil
 */
async function modifAccount() {
    let icon = document.getElementById('iconInfo').value;
    let error = document.getElementById("error")
    if (!validURL(icon)) {
        error.innerHTML = "L'URL n'est pas valide !";
    } else {
        let monIcon = document.getElementById('icon');
        monIcon.innerHTML = `<img class="rounded-circle img-thumbnail" src="${icon}" alt="icon profil"/>`;

        let jsonIcon = JSON.stringify({
            'icon': icon,
        })

        await fetch("/user/modifIcon", {
            body: jsonIcon,
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            },
            method: "POST",
        });
        error.innerHTML = "";
    }
}

/**
 * delete the current user
 */
async function deleteCurrentUser() {
    await fetch("/user/delete");
    localStorage.setItem('user', null);
    document.getElementById("navLogin").style.display = 'block';
    document.getElementById("navRegister").style.display = 'block';
    document.getElementById("drop-connect").style.display = 'none';
    backToCateg();
}

/**
 * delete the user 
 * @param {int} id 
 */
async function deleteUser(id) {
    await fetch("/user/delete/" + id);
    getUsers();
}

/**
 * view all the topics for the category 
 * @param {} elem 
 * @param {int} id 
 */
async function getTopics(elem, id) {

    let categorie = await fetch('/category/detail/' + id)
        .then(function (response) {
            return response.json();
        });

    let topics = categorie[0].topics;
    let content = document.getElementById("content");
    content.innerHTML = "";

    if (localStorage.getItem('user') != 'null') {

        let button_create = document.createElement('button');
        button_create.classList.add('btn', 'btn-primary', 'mt-2', 'ms-2');
        button_create.setAttribute('onclick', `btnCreateTopic(${id})`);
        button_create.appendChild(document.createTextNode('Créer un topic'));
        content.appendChild(button_create);
    }

    var newDiv = document.createElement("div");
    newDiv.classList.add('row');

    for (let topic of topics) {

        cardTopic(topic, content, newDiv, true);
    }

    let button = document.createElement('button');
    button.classList.add('btn', 'btn-danger', 'mt-2', 'ms-2');
    button.setAttribute("onclick", 'backToCateg()');
    button.appendChild(document.createTextNode('Retour'));
    content.appendChild(button);
}

/**
 * view all the response for the topic
 * @param {topic} elem 
 * @param {int} id 
 */
async function viewResponses(elem, id) {
    let topics = await fetch('/topic/responses/' + id)
        .then(function (response) {
            return response.json();
        });

    let responses = topics[0].reponses;

    let content = document.querySelector("#content");
    content.innerHTML = "";

    var newDiv = document.createElement("div");
    newDiv.classList.add('row');

    let topic = topics[0];

    cardTopic(topic, content, newDiv, false);

    for (let reponse of responses) {
        let card = document.createElement("div");
        card.classList.add('card');
        card.classList.add('mt-3');
        card.classList.add('p-0');

        let cardBody = document.createElement("div");
        cardBody.classList.add('card-body');

        let bodyTitle = document.createElement("h6");
        bodyTitle.classList.add('card-title', 'd-flex', 'justify-content-between', 'align-items-center');
        bodyTitle.appendChild(document.createTextNode(reponse.author));

        let pBody = document.createElement("p");
        pBody.classList.add("card-text");
        text = document.createTextNode(reponse.message);
        pBody.appendChild(text);

        let pDate = document.createElement('p');
        let divDate = document.createElement('div');
        pDate.classList.add('badge', 'rounded-pill', 'bg-secondary', 'text-end', 'p-3');
        divDate.classList.add('d-flex', 'justify-content-end');
        let date = reponse.timestamp.substring(0, 10).replaceAll('-', '/');
        let hour = reponse.timestamp.substring(11, 19);
        pDate.appendChild(document.createTextNode(date + ' ' + hour));
        divDate.appendChild(pDate);

        cardBody.appendChild(bodyTitle);
        cardBody.appendChild(pBody);
        cardBody.appendChild(divDate);
        card.appendChild(cardBody);

        newDiv.appendChild(card);
        content.appendChild(newDiv);
    }

    //if the user is not connect, he can't send a response
    if (localStorage.getItem('user') != 'null') {

        let card_reponse = document.createElement("div");
        card_reponse.classList.add('card');
        card_reponse.classList.add('mt-3');
        card_reponse.classList.add('p-0');

        let title_reponse = document.createElement("h5");
        title_reponse.classList.add('card-header', 'd-flex', 'justify-content-between', 'align-items-center');
        title_reponse.appendChild(document.createTextNode('Nouveau message'));

        let cardBody_reponse = document.createElement("div");
        cardBody_reponse.classList.add('card-body');

        let textarea_reponse = document.createElement('textarea');
        textarea_reponse.classList.add('form-control');
        textarea_reponse.setAttribute('id', 'message');

        let button__reponse = document.createElement('button');
        button__reponse.classList.add('form-control');
        button__reponse.setAttribute('id', 'send-reponse');
        button__reponse.appendChild(document.createTextNode('Envoyer'));

        button__reponse.addEventListener('click', () => newRep(topic.id));

        cardBody_reponse.appendChild(textarea_reponse);
        cardBody_reponse.appendChild(button__reponse);

        card_reponse.appendChild(title_reponse);
        card_reponse.appendChild(cardBody_reponse);

        newDiv.appendChild(card_reponse);
        content.appendChild(newDiv);
    }

    let button = document.createElement('button');
    button.classList.add('btn', 'btn-danger', 'mt-2');
    button.setAttribute("onclick", `getTopics(null,${topic.category})`);
    button.appendChild(document.createTextNode('Retour'));
    content.appendChild(button);
}

/**
 * create a response for the topic
 * @param {int} id 
 */
async function newRep(id) {
    let message = document.getElementById("message").value;

    if (message != null && message != "" && message.length != 0) {

        const reponse = JSON.stringify({ 'message': message })
        const request = await fetch("/reponse/new/" + id, {
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            },
            method: "POST",
            body: reponse
        }).then(response => response.json())

        viewResponses(null, id);
    } else {
        document.getElementById("message").classList.add("alert", "alert-danger");
    }
}

/**
 * return to the list of categ
 */
function backToCateg() {
    document.getElementById("navCateg").click();
}

/**
 * create a topic
 * @param {int} idCateg 
 */
async function createTopic(idCateg) {
    let title = document.getElementById("inputTitle").value;
    let message = document.getElementById("inputMessage").value;

    if ((title != null && title != "") && (message != null && message != "")) {
        let body = JSON.stringify({
            'title': title,
            'message': message
        });

        const request = await fetch('/topic/new/' + idCateg, {
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            },
            method: "POST",
            body: body
        }).then(response => response.json());

        getTopics(null, idCateg);
    }
}

/**
 * create a user in admin
 */
async function createUser() {
    const user = JSON.stringify({
        'password': document.getElementById("inputPassword").value,
        'username': document.getElementById("inputUsername").value,
    })

    let request = await fetch('/userCreate', {
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        },
        method: "POST",
        body: user
    }).then(response => response.json());

    if (request["success"] == 1)
        getUsers();
    else {
        document.getElementById("error").innerHTML = "L'utilisateur est déjà existant !";
    }
}

/**
 * template of the btn form-user
 */
function btncreateUser() {
    //form titre
    let div_form_titre = document.createElement('div')
    div_form_titre.classList.add('mb-3', 'row')

    let label_titre = document.createElement('label');
    label_titre.classList.add('col-sm-2', 'col-form-label')
    label_titre.appendChild(document.createTextNode('Username'));

    let div_input_titre = document.createElement('div');
    div_input_titre.classList.add('col-sm-10');

    let input_titre = document.createElement('input');
    input_titre.classList.add('form-control');
    input_titre.setAttribute('id', 'inputUsername');

    div_form_titre.appendChild(label_titre);
    div_input_titre.appendChild(input_titre);
    div_form_titre.appendChild(div_input_titre);

    //form pass
    let div_form_pass = document.createElement('div')
    div_form_pass.classList.add('mb-3', 'row')

    let label_pass = document.createElement('label');
    label_pass.classList.add('col-sm-2', 'col-form-label')
    label_pass.appendChild(document.createTextNode('Password'));

    let div_input_pass = document.createElement('div');
    div_input_pass.classList.add('col-sm-10');

    let input_pass = document.createElement('input');
    input_pass.classList.add('form-control');
    input_pass.setAttribute('type', 'password');
    input_pass.setAttribute('id', 'inputPassword');

    div_form_pass.appendChild(label_pass);
    div_input_pass.appendChild(input_pass);
    div_form_pass.appendChild(div_input_pass);

    let buttonCreate = document.createElement('button');
    buttonCreate.classList.add('btn', 'btn-primary');
    buttonCreate.setAttribute('onclick', 'createUser()');
    buttonCreate.appendChild(document.createTextNode('Créer'));

    let buttonRetour = document.createElement('button');
    buttonRetour.classList.add('btn', 'btn-danger', 'ms-2');
    buttonRetour.setAttribute('onclick', 'document.getElementById("account").click()');
    buttonRetour.appendChild(document.createTextNode('Retour'));

    let label_error = document.createElement('label');
    label_error.classList.add('text-danger', 'ms-2');
    label_error.setAttribute('id', 'error');

    let content = document.getElementById('content');
    content.innerHTML = "";

    content.appendChild(div_form_titre);
    content.appendChild(div_form_pass);
    content.appendChild(buttonCreate);
    content.appendChild(buttonRetour);
    content.appendChild(label_error);
}

/**
 * create a category
 */
async function createCategory() {
    let title = document.getElementById('inputTitle').value;
    let image = document.getElementById('inputImg').value;
    let desc = document.getElementById('inputDesc').value;

    if ((title != null && title != "") && (image != null && image != "") && (desc != null && desc != "")) {

        image = (validURL(image)) ? image : "http://www.checkinatwork.org/wp-content/uploads/2018/06/checkinatwork5-300x120.png";

        let body = JSON.stringify({
            'title': title,
            'img': image,
            'desc': desc
        })

        const request = await fetch("/category/new", {
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            },
            method: "POST",
            body: body
        }).then(response => response.json());

        if (request['success'] == 1)
            backToCateg();

    } else {
        document.getElementById('error').innerHTML = 'Erreur il faut remplir toute les données !';
    }
}

/**
 * template of the btn form-topic
 * @param {int} id 
 */
function btnCreateTopic(id) {

    //form titre
    let div_form_titre = document.createElement('div')
    div_form_titre.classList.add('mb-3', 'row')

    let label_titre = document.createElement('label');
    label_titre.classList.add('col-sm-2', 'col-form-label')
    label_titre.appendChild(document.createTextNode('Titre'));

    let div_input_titre = document.createElement('div');
    div_input_titre.classList.add('col-sm-10');

    let input_titre = document.createElement('input');
    input_titre.classList.add('form-control');
    input_titre.setAttribute('id', 'inputTitle');

    div_form_titre.appendChild(label_titre);
    div_input_titre.appendChild(input_titre);
    div_form_titre.appendChild(div_input_titre);

    //form mess
    let div_form_mess = document.createElement('div')
    div_form_mess.classList.add('mb-3', 'row')

    let label_mess = document.createElement('label');
    label_mess.classList.add('col-sm-2', 'col-form-label')
    label_mess.appendChild(document.createTextNode('Message'));

    let div_input_mess = document.createElement('div');
    div_input_mess.classList.add('col-sm-10');

    let input_mess = document.createElement('input');
    input_mess.classList.add('form-control');
    input_mess.setAttribute('id', 'inputMessage');

    div_form_mess.appendChild(label_mess);
    div_input_mess.appendChild(input_mess);
    div_form_mess.appendChild(div_input_mess);

    let buttonCreate = document.createElement('button');
    buttonCreate.classList.add('btn', 'btn-primary');
    buttonCreate.setAttribute('onclick', `createTopic(${id})`);
    buttonCreate.appendChild(document.createTextNode('Créer'));

    let buttonRetour = document.createElement('button');
    buttonRetour.classList.add('btn', 'btn-danger', 'ms-2');
    buttonRetour.setAttribute('onclick', `getTopics(null,${id})`);
    buttonRetour.appendChild(document.createTextNode('Retour'));

    let label_error = document.createElement('label');
    label_error.classList.add('text-danger');
    label_error.setAttribute('id', 'error');

    let content = document.getElementById('content');
    content.innerHTML = "";

    content.appendChild(div_form_titre);
    content.appendChild(div_form_mess);
    content.appendChild(buttonCreate);
    content.appendChild(buttonRetour);
    content.appendChild(label_error);
}

/**
 * template of the btn form-category
 */
function btncreateCateg() {
    //form titre
    let div_form_titre = document.createElement('div')
    div_form_titre.classList.add('mb-3', 'row')

    let label_titre = document.createElement('label');
    label_titre.classList.add('col-sm-2', 'col-form-label')
    label_titre.appendChild(document.createTextNode('Titre'));

    let div_input_titre = document.createElement('div');
    div_input_titre.classList.add('col-sm-10');

    let input_titre = document.createElement('input');
    input_titre.classList.add('form-control');
    input_titre.setAttribute('id', 'inputTitle');

    div_form_titre.appendChild(label_titre);
    div_input_titre.appendChild(input_titre);
    div_form_titre.appendChild(div_input_titre);

    //form img
    let div_form_img = document.createElement('div')
    div_form_img.classList.add('mb-3', 'row')

    let label_img = document.createElement('label');
    label_img.classList.add('col-sm-2', 'col-form-label')
    label_img.appendChild(document.createTextNode('Image'));

    let div_input_img = document.createElement('div');
    div_input_img.classList.add('col-sm-10');

    let input_img = document.createElement('input');
    input_img.classList.add('form-control');
    input_img.setAttribute('id', 'inputImg');

    div_form_img.appendChild(label_img);
    div_input_img.appendChild(input_img);
    div_form_img.appendChild(div_input_img);

    //form desc
    let div_form_desc = document.createElement('div')
    div_form_desc.classList.add('mb-3', 'row')

    let label_desc = document.createElement('label');
    label_desc.classList.add('col-sm-2', 'col-form-label')
    label_desc.appendChild(document.createTextNode('Description'));

    let div_input_desc = document.createElement('div');
    div_input_desc.classList.add('col-sm-10');

    let input_desc = document.createElement('input');
    input_desc.classList.add('form-control');
    input_desc.setAttribute('id', 'inputDesc');

    div_form_desc.appendChild(label_desc);
    div_input_desc.appendChild(input_desc);
    div_form_desc.appendChild(div_input_desc);

    let buttonCreate = document.createElement('button');
    buttonCreate.classList.add('btn', 'btn-primary');
    buttonCreate.setAttribute('onclick', 'createCategory()');
    buttonCreate.appendChild(document.createTextNode('Créer'));

    let buttonRetour = document.createElement('button');
    buttonRetour.classList.add('btn', 'btn-danger', 'ms-2');
    buttonRetour.setAttribute('onclick', 'backToCateg()');
    buttonRetour.appendChild(document.createTextNode('Retour'));

    let label_error = document.createElement('label');
    label_error.classList.add('text-danger');
    label_error.setAttribute('id', 'error');

    let content = document.getElementById('content');
    content.innerHTML = "";

    content.appendChild(div_form_titre);
    content.appendChild(div_form_img);
    content.appendChild(div_form_desc);
    content.appendChild(buttonCreate);
    content.appendChild(buttonRetour);
    content.appendChild(label_error);
}


/**
 * template of topic
 * @param {Topics} topic 
 * @param {document.getElementById('content')} content 
 * @param {div} newDiv 
 * @param {boolean} isResp 
 */
const cardTopic = function (topic, content, newDiv, isResp) {
    let card = document.createElement("div");
    card.classList.add('card', 'mt-3', 'p-0');

    let title = document.createElement("h5");
    title.classList.add('card-header', 'd-flex', 'justify-content-between', 'align-items-center');
    title.appendChild(document.createTextNode(topic.title));

    let views = document.createElement('span');
    let i = document.createElement('i');
    i.classList.add('fa', 'fa-eye', 'mx-2')
    views.appendChild(document.createTextNode(topic.views))
    views.appendChild(i);
    title.appendChild(views);

    let cardBody = document.createElement("div");
    cardBody.classList.add('card-body');

    let bodyTitle = document.createElement("h6");
    bodyTitle.classList.add('card-title');
    bodyTitle.appendChild(document.createTextNode('Auteur : ' + topic.author));

    let pBody = document.createElement("p");
    pBody.classList.add("card-text");
    text = document.createTextNode('Message : ' + topic.message);
    pBody.appendChild(text);

    cardBody.appendChild(bodyTitle);
    cardBody.appendChild(pBody);

    if (isResp) {
        let a = document.createElement('a');
        a.classList.add('stretched-link');
        a.setAttribute("onclick", 'viewResponses(this,' + topic.id + ')');
        a.appendChild(document.createTextNode('voir réponses'));
        cardBody.appendChild(a)
    }

    let pDate = document.createElement('p');
    let divDate = document.createElement('div');
    pDate.classList.add('badge', 'rounded-pill', 'bg-secondary', 'text-end', 'p-3');
    divDate.classList.add('d-flex', 'justify-content-end');
    let date = topic.timestamp.substring(0, 10).replaceAll('-', '/');
    let hour = topic.timestamp.substring(11, 19);
    pDate.appendChild(document.createTextNode(date + ' ' + hour));
    divDate.appendChild(pDate);

    cardBody.appendChild(divDate);
    card.appendChild(title);
    card.appendChild(cardBody);

    newDiv.appendChild(card);
    content.appendChild(newDiv);
}

/**
 * get all the user from the database
 */
async function getUsers() {
    let users = await fetch('/allUsers').then(response => response.json());
    let label = document.createElement('label');
    let content = document.getElementById("content");
    content.innerHTML = "";

    let button_create = document.createElement('button');
    button_create.classList.add('btn', 'btn-primary', 'mb-2');
    button_create.setAttribute('onclick', 'btncreateUser()');
    button_create.appendChild(document.createTextNode('Créer un utilisateur'));
    content.appendChild(button_create);

    createTable(users);
}

/**
 * table of users
 * @param {User[]} users 
 */
function createTable(users) {

    let table = document.createElement('table');
    table.classList.add('table');

    let thead = document.createElement('thead');
    let tr = document.createElement('tr');

    let th = document.createElement('th');
    th.setAttribute('scope', 'col');
    th.appendChild(document.createTextNode("id"));
    tr.appendChild(th)

    th = document.createElement('th');
    th.setAttribute('scope', 'col');
    th.appendChild(document.createTextNode("username"));
    tr.appendChild(th)

    th = document.createElement('th');
    th.setAttribute('scope', 'col');
    th.appendChild(document.createTextNode("roles"));
    tr.appendChild(th)

    th = document.createElement('th');
    th.setAttribute('scope', 'col');
    th.appendChild(document.createTextNode("Date d'inscription"));
    tr.appendChild(th)

    thead.appendChild(tr);

    table.appendChild(thead);

    let tbody = document.createElement('tbody');

    //create row for each user
    users.forEach(user => {

        if (user.roles[0] != "ROLE_ADMIN") {

            tr = document.createElement('tr');
            th = document.createElement('th');
            th.setAttribute('scope', 'row');
            th.appendChild(document.createTextNode(user.id));
            tr.appendChild(th);

            let td = document.createElement('td');
            td.appendChild(document.createTextNode(user.username));
            tr.appendChild(td);

            td = document.createElement('td');
            td.appendChild(document.createTextNode(user.roles));
            tr.appendChild(td);

            td = document.createElement('td');
            td.appendChild(document.createTextNode(user.date.date.substring(0, 10).replaceAll('-', '/')));
            tr.appendChild(td);

            td = document.createElement('td');

            let i = document.createElement('i');
            i.classList.add('fas', 'fa-trash');

            let button = document.createElement('button');
            button.classList.add('btn', 'btn-danger');
            button.setAttribute('onclick', `deleteUser(${user.id})`);
            button.appendChild(i);
            td.appendChild(button);
            tr.appendChild(td);

            tbody.appendChild(tr);

        }
    });

    table.appendChild(tbody);
    content.appendChild(table)
}


