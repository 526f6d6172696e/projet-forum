#!/bin/bash

sleep 60

cd /var/www
chmod -R 777 var
rm -r migrations/*
symfony console cache:clear
symfony console doctrine:database:create --if-not-exists
symfony console doctrine:migrations:diff --no-interaction
symfony console doctrine:migrations:migrate --no-interaction
apache2-foreground