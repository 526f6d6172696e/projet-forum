<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use DateTime;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220110141740 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Ajoute les tables reponse et topic';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE reponse (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, topic_id INTEGER NOT NULL, author_id INTEGER NOT NULL, likes INTEGER DEFAULT NULL, message CLOB NOT NULL, timestamp VARCHAR(255) NOT NULL)');
        $this->addSql('CREATE INDEX IDX_3E7B0BFB1F55203D ON reponse (topic_id)');
        $this->addSql('CREATE INDEX IDX_3E7B0BFBF675F31B ON reponse (author_id)');
        $this->addSql('CREATE TABLE topic (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, category_id INTEGER NOT NULL, creator_id INTEGER NOT NULL, message CLOB NOT NULL, timestamp VARCHAR(255) NOT NULL, likes INTEGER DEFAULT NULL, views INTEGER DEFAULT NULL)');
        $this->addSql('CREATE INDEX IDX_9D40DE1B12469DE2 ON topic (category_id)');
        $this->addSql('CREATE INDEX IDX_9D40DE1B61220EA6 ON topic (creator_id)');

        //$this->addSql('INSERT INTO topic (category_id,creator_id,message,timestamp,likes,views) VALUES (1,1,"blabla","' . new DateTime() . ',0,0)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE reponse');
        $this->addSql('DROP TABLE topic');
    }
}
