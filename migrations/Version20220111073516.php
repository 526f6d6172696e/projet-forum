<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220111073516 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP INDEX IDX_3E7B0BFBF675F31B');
        $this->addSql('DROP INDEX IDX_3E7B0BFB1F55203D');
        $this->addSql('CREATE TEMPORARY TABLE __temp__reponse AS SELECT id, topic_id, author_id, likes, message, timestamp FROM reponse');
        $this->addSql('DROP TABLE reponse');
        $this->addSql('CREATE TABLE reponse (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, topic_id INTEGER NOT NULL, author_id INTEGER NOT NULL, likes INTEGER DEFAULT NULL, message CLOB NOT NULL COLLATE BINARY, timestamp VARCHAR(255) NOT NULL, CONSTRAINT FK_5FB6DEC71F55203D FOREIGN KEY (topic_id) REFERENCES topic (id) NOT DEFERRABLE INITIALLY IMMEDIATE, CONSTRAINT FK_5FB6DEC7F675F31B FOREIGN KEY (author_id) REFERENCES user (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO reponse (id, topic_id, author_id, likes, message, timestamp) SELECT id, topic_id, author_id, likes, message, timestamp FROM __temp__reponse');
        $this->addSql('DROP TABLE __temp__reponse');
        $this->addSql('CREATE INDEX IDX_5FB6DEC71F55203D ON reponse (topic_id)');
        $this->addSql('CREATE INDEX IDX_5FB6DEC7F675F31B ON reponse (author_id)');
        $this->addSql('DROP INDEX IDX_9D40DE1B61220EA6');
        $this->addSql('DROP INDEX IDX_9D40DE1B12469DE2');
        $this->addSql('CREATE TEMPORARY TABLE __temp__topic AS SELECT id, category_id, creator_id, message, timestamp, likes, views FROM topic');
        $this->addSql('DROP TABLE topic');
        $this->addSql('CREATE TABLE topic (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, category_id INTEGER NOT NULL, creator_id INTEGER NOT NULL, message CLOB NOT NULL COLLATE BINARY, timestamp VARCHAR(255) NOT NULL, likes INTEGER DEFAULT NULL, views INTEGER DEFAULT NULL, title VARCHAR(255) NOT NULL, CONSTRAINT FK_9D40DE1B12469DE2 FOREIGN KEY (category_id) REFERENCES category (id) NOT DEFERRABLE INITIALLY IMMEDIATE, CONSTRAINT FK_9D40DE1B61220EA6 FOREIGN KEY (creator_id) REFERENCES user (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO topic (id, category_id, creator_id, message, timestamp, likes, views) SELECT id, category_id, creator_id, message, timestamp, likes, views FROM __temp__topic');
        $this->addSql('DROP TABLE __temp__topic');
        $this->addSql('CREATE INDEX IDX_9D40DE1B61220EA6 ON topic (creator_id)');
        $this->addSql('CREATE INDEX IDX_9D40DE1B12469DE2 ON topic (category_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP INDEX IDX_5FB6DEC71F55203D');
        $this->addSql('DROP INDEX IDX_5FB6DEC7F675F31B');
        $this->addSql('CREATE TEMPORARY TABLE __temp__reponse AS SELECT id, topic_id, author_id, likes, message, timestamp FROM reponse');
        $this->addSql('DROP TABLE reponse');
        $this->addSql('CREATE TABLE reponse (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, topic_id INTEGER NOT NULL, author_id INTEGER NOT NULL, likes INTEGER DEFAULT NULL, message CLOB NOT NULL, timestamp VARCHAR(255) NOT NULL)');
        $this->addSql('INSERT INTO reponse (id, topic_id, author_id, likes, message, timestamp) SELECT id, topic_id, author_id, likes, message, timestamp FROM __temp__reponse');
        $this->addSql('DROP TABLE __temp__reponse');
        $this->addSql('CREATE INDEX IDX_3E7B0BFBF675F31B ON reponse (author_id)');
        $this->addSql('CREATE INDEX IDX_3E7B0BFB1F55203D ON reponse (topic_id)');
        $this->addSql('DROP INDEX IDX_9D40DE1B12469DE2');
        $this->addSql('DROP INDEX IDX_9D40DE1B61220EA6');
        $this->addSql('CREATE TEMPORARY TABLE __temp__topic AS SELECT id, category_id, creator_id, message, timestamp, likes, views FROM topic');
        $this->addSql('DROP TABLE topic');
        $this->addSql('CREATE TABLE topic (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, category_id INTEGER NOT NULL, creator_id INTEGER NOT NULL, message CLOB NOT NULL, timestamp VARCHAR(255) NOT NULL, likes INTEGER DEFAULT NULL, views INTEGER DEFAULT NULL)');
        $this->addSql('INSERT INTO topic (id, category_id, creator_id, message, timestamp, likes, views) SELECT id, category_id, creator_id, message, timestamp, likes, views FROM __temp__topic');
        $this->addSql('DROP TABLE __temp__topic');
        $this->addSql('CREATE INDEX IDX_9D40DE1B12469DE2 ON topic (category_id)');
        $this->addSql('CREATE INDEX IDX_9D40DE1B61220EA6 ON topic (creator_id)');
    }
}
