<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220109220950 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Ajoute les tables category et user';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE category (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, title VARCHAR(255) NOT NULL, image VARCHAR(255) NOT NULL, description VARCHAR(255) NOT NULL)');
        $this->addSql('CREATE TABLE user (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, username VARCHAR(180) NOT NULL, roles CLOB NOT NULL --(DC2Type:json)
        , password VARCHAR(255) NOT NULL, date_inscription DATE NOT NULL, icone CLOB DEFAULT NULL)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D649F85E0677 ON user (username)');

        $this->addSql('INSERT INTO category (title,image,description) VALUES ("Première Catégorie", "http://www.checkinatwork.org/wp-content/uploads/2018/06/checkinatwork5-300x120.png","Test")');
        $this->addSql('INSERT INTO category (title,image,description) VALUES ("Deuxième Catégorie", "http://www.checkinatwork.org/wp-content/uploads/2018/06/checkinatwork5-300x120.png","Another Test")');
        //$this->addSql('INSERT INTO user (username, roles, date_inscription, icone) VALUES ("Romain", [] , "2022-01-11" , null)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE category');
        $this->addSql('DROP TABLE user');
    }
}
