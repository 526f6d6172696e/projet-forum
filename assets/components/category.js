$(function () {
    //const func
    const card = function (title, image, desc, id) {
        return `<div class="card m-2" style="width: 18rem;">
        <img class="card-img-top" src="${image}" alt="" width="150" height="150">
        <div class="card-body">
        <h5 class="card-title">${title}</h5>
        <p class="card-text">${desc}</p>
        <a href="#" class="stretched-link" onclick="getTopics(this,${id})" >Parcourir les sujets</a>
        </div>
        </div>`
    }

    //Show the category
    $("#navCateg").on('click', async function (event) {

        let category = await fetch('/category/')
            .then(function (response) {
                return response.json();
            });

        document.getElementById('content').innerHTML = "";

        if (localStorage.getItem('user') != 'null') {

            let button_create = document.createElement('button');
            button_create.classList.add('btn', 'btn-primary');
            button_create.setAttribute('onclick', 'btncreateCateg()');
            button_create.appendChild(document.createTextNode('Créer une catégorie'));
            document.getElementById('content').appendChild(button_create);
        }

        let content = "<div class='row'>";
        category.forEach(categ => {
            content += card(categ.title, categ.image, categ.description, categ.id)
        });

        $("#content").html($("#content").html() + content + "</div>");

        if (localStorage.getItem('user') == 'null')
            $("#createCateg").hide();
        else
            $("#createCateg").show();
    });

    $("#navCateg").trigger("click");
});