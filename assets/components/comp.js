$("#drop-connect").hide();
$("#createCateg").hide();


// Execute when the page is fully loaded
$(function () {


    const account = function (username, icon, dateInsc) {
        //if not valid url there is a default icon
        icon = validURL(icon) ? icon : "https://cdn-icons-png.flaticon.com/512/1250/1250689.png";

        return `
        <div class="card">
            <div class="row m-4">
                <div class="col-2" id="icon">
                    <img class="rounded-circle img-thumbnail" src="${icon}" alt="icon profil"/>
                </div>
                <div class="col-8 ms-8 border-start border-2 border-dark">
                    <div class="mb-3 row">
                        <label class="col-sm-3 col-form-label">Username</label>
                        <div class="col-sm-8">
                            <input type="text" readonly class="form-control disabled" value="${username}">
                        </div>
                    </div>
                    <div class="mb-3 row d-none">
                        <label class="col-sm-3 col-form-label">Password</label>
                        <div class="col-sm-8">
                            <input type="password" class="form-control" id="inputPassword">
                        </div>
                    </div>
                    <div class="mb-3 row">
                        <label class="col-sm-3 col-form-label">Date d'inscription</label>
                        <div class="col-sm-8">
                            <input type="text" readonly class="form-control disabled" value="${dateInsc.substring(0, 10).replaceAll('-', '/')}">
                        </div>
                    </div>
                    <div class="mb-3 row">
                        <label class="col-sm-3 col-form-label">Changer d'icône</label>
                        <div class="col-sm-8">
                            <input class="form-control" type="text" id="iconInfo">
                        </div>
                        <label class="col-form-label text-danger" id="error"></label>
                    </div>
                </div>
            </div>
            <div class="container d-flex justify-content-center">
                <button type="button" class="btn btn-primary m-2" onclick="modifAccount()">Enregistrer</button>
                <button type="button" class="btn btn-danger m-2" onclick="deleteCurrentUser()">Supprimer le compte</button>
            </div>
        </div>`;
    }

    //Show register
    $("#navRegister").on('click', function (event) {
        let content =
            `<div class="container py-5 h-100">
                <div class="row d-flex justify-content-center align-items-center h-100">
                    <div class="col-12 col-md-8 col-lg-6 col-xl-5">
                        <div class="card shadow-2-strong" style="border-radius: 1rem;">
                            <div class="card-body p-5 text-center">
                                <h3 class="mb-5">Inscription</h3>
                                <form id="registerForm">
                                    <div class="form-outline mb-4">
                                        <input type="text" class="form-control" id="inputUser" required>
                                        <label class="form-label">Username</label>
                                    </div>
                        
                                    <div class="form-outline mb-4">
                                        <input type="password" class="form-control" id="inputPass" required>
                                        <label class="form-label">Password</label>
                                    </div>
                                    <button class="btn btn-primary btn-lg btn-block" type="submit">Inscription</button>
                                </form>
                            </div<
                        </div>
                    </div>
                </div>
            </div>`

        $("#content").html(content);


        $("#registerForm").on('submit', async function (event) {
            event.preventDefault();

            const user = JSON.stringify({ 'password': $("#inputPass").val(), 'username': $("#inputUser").val() })

            const request = await fetch("/register", {
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json'
                },
                method: "POST",
                body: user
            }).then(response => response.json())

            if (request["success"] == 0) $("#registerForm").html("<p class='text-danger'>L'identifiant est déjà utilisé</p>" + $("#registerForm").html());
            else {
                $("#user").text(request['nom']);
                $("#navCateg").trigger('click');
                $("#drop-connect").show();
                $("#navLogin").hide();
                $("#navRegister").hide();
                $("#createCateg").show();

                localStorage.setItem('user', request['nom']);
            }
        });

    });


    $("#navLogin").on('click', function (event) {
        let content =
            `<div class="container py-5 h-100">
                <div class="row d-flex justify-content-center align-items-center h-100">
                    <div class="col-12 col-md-8 col-lg-6 col-xl-5">
                        <div class="card shadow-2-strong" style="border-radius: 1rem;">
                            <div class="card-body p-5 text-center">
                                <h3 class="mb-5">Connexion</h3>
                                <form id="loginForm">
                                    <div class="form-outline mb-4">
                                        <input type="text" class="form-control" id="inputUser" required>
                                        <label class="form-label">Username</label>
                                    </div>
                        
                                    <div class="form-outline mb-4">
                                        <input type="password" class="form-control" id="inputPass" required>
                                        <label class="form-label">Password</label>
                                    </div>
                                    <button class="btn btn-primary btn-lg btn-block" type="submit" id="loginButton">Login</button>
                                </form>
                            </div<
                        </div>
                    </div>
                </div>
            </div>`

        $("#content").html(content);

        $("#loginButton").on('click', async function (event) {
            event.preventDefault();

            const user = JSON.stringify({
                'password': $("#inputPass").val(),
                'username': $("#inputUser").val()
            });

            const request = await fetch("/login", { method: "POST", body: user })
                .then(response => response.json());

            if (request["success"] == 0)
                $("#loginForm").html("<p class='text-danger'>Mauvais identifiants/mot de passe</p>" + $("#loginForm").html()), error => console.log(error);
            else {
                console.log(request);
                $("#navCateg").trigger('click');
                $("#user").text(request["nom"]);
                $("#drop-connect").show();
                $("#navLogin").hide();
                $("#navRegister").hide();
                $("#createCateg").show();

                localStorage.setItem('user', request["nom"]);
            }
        });
    });

    $("#navLogout").on('click', async function (event) {

        $.ajax({
            url: '/logout',
            success: function (event) {
                localStorage.setItem('user', null);
                $("#navLogin").show();
                $("#navRegister").show();
                $("#drop-connect").hide();
                $("#createCateg").hide();
                $("#navCateg").trigger('click');
            },
            error: function (error) {
                alert('Error disconnected')
                console.log("Error : ", error);
            }
        })
    });

    $("#account").on('click', async function (event) {
        $("#createCateg").hide();
        let request = await fetch('/user').then(response => response.json());

        $("#content").html("");

        if (request.roles[0] == "ROLE_ADMIN") {

            let button_list = document.createElement('button');
            button_list.classList.add('btn', 'btn-primary', 'mb-2');
            button_list.setAttribute('onclick', 'getUsers()');
            button_list.appendChild(document.createTextNode('Voir liste utilisateurs'));
            document.getElementById('content').appendChild(button_list);
        }

        $("#content").html($("#content").html() + account(request["username"], request["icon"], request["dateInsc"]));
    })

    //get the username of the current user
    var username = localStorage.getItem('user');
    //if it's not null => a user is connected
    if (username != 'null') {
        $("#user").text(username);
        $("#navLogin").hide();
        $("#navRegister").hide();
        $("#drop-connect").show();
    }
})