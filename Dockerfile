FROM php:7.4-apache

#installation des outils unix et des extensions php
RUN apt-get update && apt-get install -my wget dos2unix
RUN apt-get -y install npm nodejs
RUN docker-php-ext-install mysqli pdo pdo_mysql

#ajout de composer
RUN wget https://getcomposer.org/download/2.0.9/composer.phar && \
    mv composer.phar /usr/bin/composer && \ 
    chmod +x /usr/bin/composer

#ajout de symfony
RUN wget https://get.symfony.com/cli/installer -O - | bash && \
    mv /root/.symfony/bin/symfony /usr/bin/symfony && \
    chmod +x /usr/bin/symfony

#configuration apache
COPY ./apache.conf /etc/apache2/sites-enabled/000-default.conf

#activation et redémarrage apache
RUN a2enmod rewrite && service apache2 restart

RUN rm node_modules

COPY . /var/www

WORKDIR /var/www/

#installation des dépendances du projet
RUN composer install
RUN npm i
RUN npm run dev

#utilisation de init.sh pour lancer les migrations symfony
COPY ./init.sh /init.sh
RUN chmod +x /init.sh
RUN dos2unix /init.sh

CMD ["/init.sh"]